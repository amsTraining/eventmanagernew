-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: group2db
-- ------------------------------------------------------
-- Server version	5.7.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `events` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(50) NOT NULL,
  `START` datetime NOT NULL,
  `END` datetime NOT NULL,
  `room_id` int(11) NOT NULL,
  `GROUP_ID` int(11) DEFAULT NULL,
  `DETAIL` text,
  `REGISTERED_BY` int(11) NOT NULL,
  `CREATED` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (28,'井手のためのJava研修','2019-06-05 11:38:54','2019-06-05 11:38:54',1,1,'詳細',1,'2019-06-05 11:38:54'),(29,'井手のためのSQL研修','2019-06-05 11:38:54','2019-06-05 11:38:54',1,1,'詳細',1,'2019-06-05 11:38:54'),(30,'井手のためのプログラミング研修','2019-06-05 11:38:54','2019-06-05 11:38:54',1,1,'詳細',1,'2019-06-05 11:38:54'),(31,'井手のためのhtml研修','2019-06-05 11:38:54','2019-06-05 11:38:54',1,1,'詳細',1,'2019-06-05 11:38:54'),(32,'井手のためのプログラミング研修','2019-06-05 11:38:54','2019-06-05 11:38:54',1,1,'詳細',1,'2019-06-05 11:38:54'),(33,'井手のためのプログラミング研修','2019-06-05 11:38:54','2019-06-05 11:38:54',1,1,'詳細',1,'2019-06-05 11:38:54'),(35,'井手のためのSQL研修','2019-06-06 09:34:43','2019-06-06 09:34:43',1,1,'詳細',1,'2019-06-06 09:34:43'),(36,'井手のためのプログラミング研修','2019-06-06 09:34:43','2019-06-06 09:34:43',1,1,'詳細',1,'2019-06-06 09:34:43'),(37,'井手のためのhtml研修','2019-06-06 09:34:43','2019-06-06 09:34:43',1,1,'詳細',1,'2019-06-06 09:34:43'),(38,'井手のためのプログラミング研修','2019-06-06 09:34:43','2019-06-06 09:34:43',1,1,'詳細',1,'2019-06-06 09:34:43'),(39,'井手のためのプログラミング研修','2019-06-06 09:34:43','2019-06-06 09:34:43',1,1,'詳細',1,'2019-06-06 09:34:43');
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-06 17:01:27
