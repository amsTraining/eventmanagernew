package dao;

import java.util.List;

import domain.Attend;
import domain.Event;

public interface EventDao {

	public List<Event> findAll(Event event, List<Attend> attendEvent) throws Exception;
	//	イベントの一覧を表示させます。

	public List<Event> findByToday(Event event, List<Attend> attendEvent) throws Exception;
	//	今日のイベントを表示させます。

	public Event findByEventId(Event event) throws Exception;

	//イベントの詳細を表示させます。
	public List<Event> findAttendName(Event event) throws Exception;
	//イベントの参加者を表示します。

	public void insert(Event event) throws Exception;
	//	イベントを登録します。

	public void update(Event event) throws Exception;
	//	イベントを編集します。

	public void delete(Event event) throws Exception;
	//	イベントを削除します。

	public Event findByGroupId(Event event) throws Exception;

	public Event Count() throws Exception;

	public Event CountToday() throws Exception;

	public void deleteFindUserId(Attend attend) throws Exception;

	public boolean checkDate(String strDate) throws Exception;

}
