package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import domain.Attend;
import domain.Event;
import domain.Login;

public class AttendDaoImpl implements AttendDao {
	private DataSource ds;

	public AttendDaoImpl(DataSource ds) {
		this.ds = ds;
	}

	@Override
	public void insert(Attend attend) throws Exception {
		try (Connection con = ds.getConnection()) {
			String sql = "insert into attends (attends.user_id,attends.event_id) values(?,?)";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setObject(1, attend.getAtttendUserId(), Types.INTEGER);
			stmt.setObject(2, attend.getAttendEventId(), Types.INTEGER);
			stmt.executeUpdate();

		}

		// TODO 自動生成されたメソッド・スタブ

	}

	@Override
	public void delete(Attend attend) throws Exception {
		// TODO 自動生成されたメソッド・スタブ
		try (Connection con = ds.getConnection()) {
			String sql = "select * from attends  where attends.user_id=? And attends.event_id=?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setObject(1, attend.getAtttendUserId(), Types.INTEGER);
			stmt.setObject(2, attend.getAttendEventId(), Types.INTEGER);
			ResultSet rs = stmt.executeQuery();
			rs.next();
			mapToAttendA(rs);

		}

	}

	private Attend mapToAttendA(ResultSet rs) throws SQLException {
		Attend attend = new Attend();
		//		attend.setAtttendUserId((Integer) rs.getObject("attendUserId"));
		//		attend.setAttendEventId((Integer) rs.getObject("attendGroupId"));
		attend.setAttendEventId((Integer) rs.getObject("event_id"));

		return attend;
	}

	private Attend mapToAttendB(ResultSet rs) throws SQLException {
		Attend attend = new Attend();
		//		attend.setAttendId((Integer) rs.getObject("user_id"));
		attend.setAttendEventId((Integer) rs.getObject("event_id"));//event_idをInteger型でセット

		return attend;
	}

	private Login mapToAttendC(ResultSet rs) throws SQLException {
		Login login = new Login();
		login.setGroupId((Integer) rs.getObject("group_id"));

		return login;
	}

	@Override
	public List<Attend> findByUserId(Attend attend) throws Exception {
		// TODO 自動生成されたメソッド・スタブ
		List<Attend> attendList = new ArrayList<>();
		try (Connection con = ds.getConnection()) {
			String sql = "select * from attends  where attends.user_id=? ";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setObject(1, attend.getAtttendUserId(), Types.INTEGER);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				attendList.add(mapToAttendB(rs));
			}
		}
		return attendList;
	}

	@Override
	public List<Attend> findByLoginId(Attend attend) throws Exception {
		// TODO 自動生成されたメソッド・スタブ
		List<Attend> attendList = new ArrayList<>();
		try (Connection con = ds.getConnection()) {
			String sql = "SELECT " +
					" e.name as name, a.user_id as user_id, a.event_id as event_id" +
					" FROM " +
					" attends AS a" +
					" JOIN" +
					" users AS u ON a.user_id = u.id" +
					" JOIN" +
					" employee AS e ON u.employee_id = e.id" +
					" WHERE" +
					" u.login_id = ?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, attend.getLoginId());//？にログインしているユーザのloginIdを入れる
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {//次のデータがなくなったらfalse
				attendList.add(mapToAttendB(rs));//データを1つずつ追加
			}

		}
		return attendList;

	}

	@Override
	public List<Attend> findByEventListId(Integer userId) throws Exception {
		// TODO 自動生成されたメソッド・スタブ
		try (Connection con = ds.getConnection()) {
			List<Attend> userEventList = new ArrayList<>();

			String sql = "select * from attends  where attends.user_id=?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setObject(1, userId, Types.INTEGER);

			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				userEventList.add(mapToAttendA(rs));
			}
			return userEventList;
		}
	}

	@Override
	public Attend ifAttendByEventId(List<Attend> attend) throws Exception {
		// TODO 自動生成されたメソッド・スタブ

		return null;
	}

	@Override
	public boolean joining(Attend attend) throws Exception {
		boolean joining;
		try (Connection con = ds.getConnection()) {
			String sql = "select * from attends  where attends.user_id=? And attends.event_id=?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setObject(1, attend.getAtttendUserId(), Types.INTEGER);
			stmt.setObject(2, attend.getAttendEventId(), Types.INTEGER);

			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				joining = true;
			} else {
				joining = false;
			}

		}
		return joining;
	}

	@Override
	public void attendDelete(Attend attend) throws Exception {
		// TODO 自動生成されたメソッド・スタブ
		try (Connection con = ds.getConnection()) {
			String sql = "delete from attends  where attends.user_id=? And attends.event_id=?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setObject(1, attend.getAtttendUserId(), Types.INTEGER);
			stmt.setObject(2, attend.getAttendEventId(), Types.INTEGER);
			stmt.executeUpdate();
		}
	}

	@Override
	public void attendDeleteEventId(Attend attend) throws Exception {
		try (Connection con = ds.getConnection()) {
			String sql = "delete from attends  where attends.event_id=?";
			PreparedStatement stmt = con.prepareStatement(sql);

			stmt.setObject(1, attend.getAttendEventId(), Types.INTEGER);
			stmt.executeUpdate();
		}

	}

	@Override
	public void attendDeleteUserId(Attend attend) throws Exception {
		try (Connection con = ds.getConnection()) {
			String sql = "delete from attends  where attends.user_id=?";
			PreparedStatement stmt = con.prepareStatement(sql);

			stmt.setObject(1, attend.getAtttendUserId(), Types.INTEGER);
			stmt.executeUpdate();
		}

	}

	@Override
	public boolean ifAttendGroup(Event event) throws Exception {
		// TODO 自動生成されたメソッド・スタブ
		boolean ifAttendGroup;
		Login login = new Login();
		try (Connection con = ds.getConnection()) {
			String sql = "select group_id from users  where login_id=?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, event.getLoginId());

			ResultSet rs = stmt.executeQuery();
			rs.next();
			mapToAttendC(rs);

			if (login.getGroupId() == event.getGroupId()) {
				ifAttendGroup = true;
			} else {
				ifAttendGroup = false;
			}

		}

		return ifAttendGroup;
	}

}
