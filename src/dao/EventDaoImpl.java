package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import domain.Attend;
import domain.Event;

public class EventDaoImpl implements EventDao {

	private DataSource ds;

	public EventDaoImpl(DataSource ds) {
		this.ds = ds;
	}

	/**
	 *
	 *
	 * */

	@Override
	public List<Event> findAll(Event event, List<Attend> attendEvent) throws Exception {
		// TODO 自動生成されたメソッド・スタブ
		Integer p = 0;
		if (event.getPageCount() != null) {
			p = p + (event.getPageCount() - 1) * 5;
		}

		List<Event> eventList = new ArrayList<>();
		try (Connection con = ds.getConnection()) {
			String sql = "select events.id,events.title,events.start," +
					" rooms.id as roomId,rooms.name as place,groups.id as groupId,groups.name as groupName " +
					" from " +
					" rooms join events on rooms.id=events.room_id  " +
					" join groups on events.group_id = groups.id " +
					" order by events.start desc " +
					" limit ?,5";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setObject(1, p, Types.INTEGER);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				eventList.add(mapToEventA(rs, attendEvent));
			}
		}

		return eventList;
	}

	@Override
	public Event findByEventId(Event event) throws Exception {
		// TODO 自動生成されたメソッド・スタブ
		Event eventDe = new Event();
		try (Connection con = ds.getConnection()) {
			String sql = "select events.id,events.title,events.start,events.end," +
					"rooms.name as place,events.group_id," +
					"groups.name as groupName,events.detail,registered_by," +
					"employee.name as registeredByName " +
					" FROM " +
					" rooms join events on rooms.id=events.room_id JOIN groups ON events.group_id = groups.id " +
					" JOIN users ON events.registered_by=users.id " +
					" JOIN employee ON users.employee_id = employee.id" +
					" WHERE events.id =?;";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setObject(1, event.getEventId(), Types.INTEGER);
			ResultSet rs = stmt.executeQuery();
			rs.next();
			eventDe = mapToEventC(rs);
		}
		return eventDe;
	}

	@Override
	public List<Event> findByToday(Event event, List<Attend> attendEvent) throws Exception {
		// TODO 自動生成されたメソッド・スタブ
		Integer p = 0;
		if (event.getPageCount() != null) {
			p = p + (event.getPageCount() - 1) * 5;
		}
		List<Event> eventList = new ArrayList<>();
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
		Calendar now = Calendar.getInstance();
		String today = fmt.format(now.getTime());
		now.add(Calendar.DATE, 1);
		String tomorrow = fmt.format(now.getTime());

		try (Connection con = ds.getConnection()) {
			String sql = "SELECT " +
					" events.id," +
					" events.title," +
					" events.start," +
					" rooms.name as place," +
					" groups.name AS groupName " +
					" FROM" +
					" rooms " +
					" JOIN " +
					" events ON rooms.id = events.room_id " +
					" JOIN" +
					" groups ON events.group_id = groups.id  " +
					" where events.start >= DATE_FORMAT(?,'%Y-%m-%d %H:%i:%s') " +
					" And events.start < DATE_FORMAT(?,'%Y-%m-%d %H:%i:%s') " +
					" limit ?,5";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, today);
			stmt.setString(2, tomorrow);
			stmt.setObject(3, p, Types.INTEGER);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				eventList.add(mapToEventT(rs, attendEvent));
			}
		}

		return eventList;

	}

	@Override
	public void insert(Event event) throws Exception {
		// TODO 自動生成されたメソッド・スタブ
		SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		String start = sdFormat.format(event.getStart());
		String end = sdFormat.format(event.getEnd());

		try (Connection con = ds.getConnection()) {
			String sql = "INSERT INTO events"
					+ "(title,start,end,place,group_id,detail,registered_by,created)"
					+ "VALUES(?,?,?,?,?,?,?,now())";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, event.getTitle());
			stmt.setString(2, start);
			stmt.setString(3, end);
			stmt.setString(4, event.getPlace());
			stmt.setObject(5, event.getGroupId());
			stmt.setString(6, event.getDetail());
			stmt.setObject(7, event.getUserId());
			stmt.executeUpdate();

		}

	}

	@Override
	public void update(Event event) throws Exception {
		// TODO 自動生成されたメソッド・スタブ
		SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		String start = sdFormat.format(event.getStart());
		String end = sdFormat.format(event.getEnd());
		try (Connection con = ds.getConnection()) {
			String sql = "update events set title = ?,start=?,end=?,place=?,"
					+ "group_id=?,detail=?,registered_by=?,created=now() "
					+ "where id=?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, event.getTitle());
			stmt.setString(2, start);
			stmt.setString(3, end);
			stmt.setString(4, event.getPlace());
			stmt.setObject(5, event.getGroupId(), Types.INTEGER);
			stmt.setString(6, event.getDetail());
			stmt.setObject(7, event.getRegisteredById(), Types.INTEGER);
			stmt.setObject(8, event.getEventId(), Types.INTEGER);
			stmt.executeUpdate();
		}

	}

	@Override
	public void delete(Event event) throws Exception {
		// TODO 自動生成されたメソッド・スタブ
		try (Connection con = ds.getConnection()) {
			String sql = "delete from events where id=?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setObject(1, event.getEventId(), Types.INTEGER);
			stmt.executeUpdate();
		}

	}

	@Override
	public void deleteFindUserId(Attend attend) throws Exception {
		try (Connection con = ds.getConnection()) {
			String sql = "delete from events where registered_by=?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setObject(1, attend.getAtttendUserId(), Types.INTEGER);
			stmt.executeUpdate();
		}

	}

	private Event mapToEventA(ResultSet rs, List<Attend> attendEvent) throws SQLException {
		Event event = new Event();
		Integer id = (Integer) rs.getObject("id");
		for (Attend s : attendEvent) {

			if (s.getAttendEventId() == id) {
				event.setAttendtf(true);
			}
		}
		;

		event.setEventId((Integer) rs.getObject("Id"));
		event.setTitle(rs.getString("title"));
		event.setStart((Date) rs.getObject("start"));
		event.setGroupId((Integer) rs.getObject("groupId"));
		event.setRoomId((Integer) rs.getObject("roomId"));
		event.setPlace(rs.getString("place"));
		event.setGroupName(rs.getString("groupName"));
		return event;
	}

	private Event mapToEventB(ResultSet rs) throws SQLException {
		Event event = new Event();
		event.setAttendName(rs.getString("attendName"));
		return event;
	}

	private Event mapToEventC(ResultSet rs) throws SQLException {
		Event event = new Event();
		event.setEventId((Integer) rs.getObject("id"));
		event.setTitle(rs.getString("title"));
		event.setStart((Date) rs.getObject("start"));
		event.setEnd((Date) rs.getObject("end"));
		event.setPlace(rs.getString("place"));
		event.setDetail(rs.getString("detail"));
		event.setRegisteredById((Integer) rs.getObject("registered_by"));
		event.setRegisteredByName(rs.getString("registeredByName"));
		//		event.setAttendName((String) rs.getObject("attendName"));
		event.setGroupId((Integer) rs.getObject("group_id"));
		event.setGroupName(rs.getString("groupName"));
		return event;
	}

	private Event mapToEventD(ResultSet rs) throws SQLException {
		Event event = new Event();
		event.setGroupId((Integer) rs.getObject("Id"));
		return event;
	}

	private Event mapToEventT(ResultSet rs, List<Attend> attendEvent) throws SQLException {
		Event event = new Event();

		Integer id = (Integer) rs.getObject("id");
		for (Attend s : attendEvent) {

			if (s.getAttendEventId() == id) {
				event.setAttendtf(true);
			}
		}
		;

		event.setEventId((Integer) rs.getObject("id"));
		event.setTitle(rs.getString("title"));
		event.setStart((Date) rs.getObject("start"));
		//		event.setEnd((Date) rs.getObject("end"));
		event.setPlace(rs.getString("place"));
		//		event.setDetail(rs.getString("detail"));
		//		event.setRegisteredByName(rs.getString("registeredByName"));
		//		event.setAttendName((String[]) rs.getObject("attendName"));
		event.setGroupName(rs.getString("groupName"));
		return event;
	}

	//	private Event mapToEventE(ResultSet rs) throws SQLException {
	//		Event event = new Event();
	//		event.setRow((Integer) rs.getObject("C"));
	//		return event;
	//	}

	@Override
	public List<Event> findAttendName(Event event) throws Exception {
		// TODO 自動生成されたメソッド・スタブ
		List<Event> eventList = new ArrayList<>();
		try (Connection con = ds.getConnection()) {
			String sql = "select employee.name as attendName" +
					" from attends join users " +
					"on attends.user_id=users.id " +
					" JOIN" +
					" employee ON users.employee_id = employee.id" +
					" where attends.event_id=?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setObject(1, event.getEventId(), Types.INTEGER);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				eventList.add(mapToEventB(rs));
			}

		}
		return eventList;
	}

	@Override
	public Event findByGroupId(Event event) throws Exception {
		// TODO 自動生成されたメソッド・スタブ
		Event eventGN = new Event();
		try (Connection con = ds.getConnection()) {
			String sql = "select id from groups where name=?";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, event.getGroupName());
			ResultSet rs = stmt.executeQuery();
			rs.next();
			eventGN = mapToEventD(rs);
		}
		return eventGN;

	}

	@Override
	public Event Count() throws Exception {
		// TODO 自動生成されたメソッド・スタブ
		Event eventCo = new Event();
		try (Connection con = ds.getConnection()) {
			String sql = "select count(*) as cnt from events join groups " +
					"on events.group_id = groups.id ";
			PreparedStatement stmt = con.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				eventCo.setRow(rs.getInt("cnt"));
			}
			//			rs.last();
			//			int row = rs.getRow();
			//			eventCo.setRow(row);
			//			rs.beforeFirst();
			//			//			eventCo = mapToEventE(eventCo);
		}
		return eventCo;

	}

	@Override
	public Event CountToday() throws Exception {
		// TODO 自動生成されたメソッド・スタブ
		Event eventCo = new Event();
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
		Calendar now = Calendar.getInstance();
		String today = fmt.format(now.getTime());
		now.add(Calendar.DATE, 1);
		String tomorrow = fmt.format(now.getTime());
		try (Connection con = ds.getConnection()) {
			String sql = "select * from events " +
					"where events.start >= DATE_FORMAT(?,'%Y-%m-%d %H:%i:%s') " +
					"And events.start < DATE_FORMAT(?,'%Y-%m-%d %H:%i:%s') ";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, today);
			stmt.setString(2, tomorrow);
			ResultSet rs = stmt.executeQuery();
			rs.last();
			int row = rs.getRow();
			eventCo.setRow(row);
			rs.beforeFirst();
			//			eventCo = mapToEventE(eventCo);
		}
		return eventCo;
	}

	@Override
	public boolean checkDate(String strDate) throws Exception {
		// TODO 自動生成されたメソッド・スタブ
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		sdf.setLenient(false);
		try {
			sdf.parse(strDate);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
