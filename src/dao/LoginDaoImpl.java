package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.mindrot.jbcrypt.BCrypt;

import domain.Login;

public class LoginDaoImpl implements LoginDao {

	private DataSource ds;

	public LoginDaoImpl(DataSource ds) {
		this.ds = ds;
	}

	@Override
	public Login findByLoginIdAndLoginPass(String loginId, String loginPass) throws Exception {
		// TODO 自動生成されたメソッド・スタブ
		Login login = null;

		try (Connection con = ds.getConnection()) {
			String sql = "SELECT u.id as id," +
					" u.login_id as login_id, u.login_pass as login_pass, e.name as name, " +
					"u.type_id as type_id, e.group_id as group_id" +
					" FROM " +
					" users AS u" +
					" JOIN" +
					" employee AS e ON u.employee_id = e.id" +
					" WHERE u.login_id=?";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, loginId);
			ResultSet rs = stmt.executeQuery();

			if (rs.next()) {
				if (BCrypt.checkpw(loginPass, rs.getString("login_pass"))) {
					login = mapToEvent(rs);
				}
			}
		}
		return login;

	}

	private Login mapToEvent(ResultSet rs) throws SQLException {
		Login login = new Login();
		login.setUserId((Integer) rs.getObject("id"));
		login.setLoginId(rs.getString("login_id"));
		login.setUserName(rs.getString("name"));
		login.setTypeId((Integer) rs.getObject("type_id"));
		login.setGroupId((Integer) rs.getObject("group_id"));

		return login;
	}

}
