﻿package controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DaoFactory;
import dao.EventDao;
import domain.Event;

/**
 * Servlet implementation class EventRegistServlet
 */
@WebServlet("/EventRegistServlet")
public class EventRegistServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EventRegistServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		System.out
				.print("groupは受け取れているの?" + request.getParameter("setGroup") + " : " + request.getParameter("setRoom"));

		// TODO Auto-generated method stub
		request.setAttribute("err", 0);
		request.setAttribute("err2", 0);
		request.getRequestDispatcher("WEB-INF/view/eventRegist.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String title = request.getParameter("title").trim();
		String start = request.getParameter("start").trim();
		String end = request.getParameter("end").trim();
		String place = request.getParameter("place").trim();
		String groupName = request.getParameter("groupName").trim();
		String detail = request.getParameter("detail").trim();
		Integer userId = (Integer) request.getSession().getAttribute("loginUserId");
		Integer groupId = 1;
		Event group = new Event();
		group.setGroupName(groupName);
		EventDao GroupDao = DaoFactory.createEventDao();

		try {
			Event id = GroupDao.findByGroupId(group);
			groupId = id.getGroupId();
		} catch (Exception e3) {
			// TODO 自動生成された catch ブロック
			e3.printStackTrace();
		}

		SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

		Event event = new Event();
		event.setTitle(title);
		Date date1 = new Date();
		Date date2 = new Date();
		event.setPlace(place);
		event.setUserId(userId);
		event.setDetail(detail);
		event.setGroupId(groupId);

		//		try {
		//			sdFormat.parse(start);
		//			try {
		//				sdFormat.parse(end);
		//
		//			} catch (Exception e) {
		//				// TODO: handle exception
		//				request.setAttribute("err2", -3);
		//				request.getRequestDispatcher("WEB-INF/view/eventRegist.jsp").forward(request, response);
		//			}
		//
		//		} catch (Exception e) {
		//			// TODO: handle exception
		//			request.setAttribute("err", -3);
		//			try {
		//				sdFormat.parse(end);
		//				request.getRequestDispatcher("WEB-INF/view/eventRegist.jsp").forward(request, response);
		//
		//			} catch (Exception e2) {
		//				// TODO: handle exception
		//				request.setAttribute("err2", -3);
		//				request.getRequestDispatcher("WEB-INF/view/eventRegist.jsp").forward(request, response);
		//			}
		//
		//		}
		EventDao datech = DaoFactory.createEventDao();
		try {
			if (!datech.checkDate(start) || !datech.checkDate(end)) {

				if (!datech.checkDate(start) && !datech.checkDate(end)) {
					request.setAttribute("err", -3);
					request.setAttribute("err2", -3);
				} else if (!datech.checkDate(start)) {
					request.setAttribute("err", -3);
					request.setAttribute("err2", 0);
				} else if (!datech.checkDate(end)) {
					request.setAttribute("err", 0);
					request.setAttribute("err2", -3);
				}
				request.getRequestDispatcher("WEB-INF/view/eventRegist.jsp").forward(request, response);
			}
		} catch (

		Exception e1) {
			// TODO 自動生成された catch ブロック
			e1.printStackTrace();
		}

		try {

			date1 = sdFormat.parse(start);
			event.setStart(date1);
			date2 = sdFormat.parse(end);
			event.setEnd(date2);

		} catch (ParseException e2) {
			// TODO 自動生成された catch ブロック
			e2.printStackTrace();
		}
		//		event.setRegisteredById(registered_by);

		Date now = new Date();
		if (date1.before(now) || date2.before(now) || date2.before(date1)) {
			if (date1.before(now) && date2.before(now)) {
				request.setAttribute("err", -1);
				request.setAttribute("err2", -1);
			} else if (date1.before(now)) {
				request.setAttribute("err", -1);
				request.setAttribute("err2", 0);
			} else if (date2.before(now)) {
				request.setAttribute("err", 0);
				request.setAttribute("err2", -1);
			} else if (date2.before(date1)) {
				request.setAttribute("err", 0);
				request.setAttribute("err2", -2);
			}
			request.getRequestDispatcher("WEB-INF/view/eventRegist.jsp").forward(request, response);
		} else {
			try {
				EventDao EventDao = DaoFactory.createEventDao();
				EventDao.insert(event);
				request.getRequestDispatcher("WEB-INF/view/eventRegistComp.jsp").forward(request, response);
			} catch (Exception e) {
				throw new ServletException(e);

			}
		}
	}

}
