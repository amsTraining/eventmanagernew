package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DaoFactory;
import dao.UserDao;
import domain.User;

/**
 * Servlet implementation class UserRegistServlet
 */
@WebServlet("/UserRegistServlet")
public class UserRegistServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String errMsg;
	private static final String DUPLICATE = "登録済みのログインIDのため登録ができません";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserRegistServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 * 登録画面の表示
	 * アカウント無しの社員を選択させます
	 *
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//アカウントなしの社員情報をJSPに送信します
		UserDao userDao = DaoFactory.createUserDao();
		List<User> unSetAccountList = null;
		try {
			unSetAccountList = userDao.unSetAccount();
		} catch (Exception e) {
			e.printStackTrace();
		}
		//最上段にデフォルト値をセットします
		User defaulUser = new User();
		defaulUser.setEmployeeId("社員IDを選択してください");
		defaulUser.setGroupName("");
		defaulUser.setName("");
		unSetAccountList.add(0, defaulUser);

		request.setAttribute("unSetAccountList", unSetAccountList);

		request.setAttribute("errMsg", this.errMsg);
		request.getRequestDispatcher("WEB-INF/view/userRegist.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 *
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		UserDao userDao = DaoFactory.createUserDao();

		String loginPass = request.getParameter("login_pass");
		String loginId = request.getParameter("login_id");
		Integer typeId = new Integer(request.getParameter("type_id"));
		String employeeId = request.getParameter("employee_id").split(",")[2];

		try {
			//ログインID重複チェックがOKであれば登録する
			if (!userDao.chkLoginId(loginId)) {
				errMsg = DUPLICATE;//ログインID重複
				doGet(request, response);
			}

			User user = new User();

			user.setLogin_id(loginId);
			user.setPass(loginPass);
			user.setTypeId(typeId);
			user.setEmployeeId(employeeId);
			userDao.insert(user);

			request.setAttribute("errMsg", this.errMsg);//エラー表示用
			request.getRequestDispatcher("WEB-INF/view/userRegistComp.jsp").forward(request, response);
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

}
