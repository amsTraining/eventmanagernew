package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AttendDao;
import dao.DaoFactory;
import dao.EventDao;
import dao.UserDao;
import domain.Attend;
import domain.Event;
import domain.User;

/**
 * Servlet implementation class UserEditServlet
 */
@WebServlet("/UserEditServlet")
public class UserEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserEditServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		Integer userId = Integer.valueOf(request.getParameter("userId"));
		try {
			UserDao userDao = DaoFactory.createUserDao();
			request.setAttribute("user", userDao.findByUserId(userId));
			request.setAttribute("err", 0);

			request.getRequestDispatcher("WEB-INF/view/userEdit.jsp").forward(request, response);
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		UserDao userDao = DaoFactory.createUserDao();
		Integer userId = Integer.parseInt(request.getParameter("userId"));
		String name = request.getParameter("name");
		Integer typeId = Integer.parseInt(request.getParameter("typeId"));
		String loginId = null;
		//エラー表示用の変数初期化
		int err = 0;
		request.setAttribute("err", err);
		try {
			//ユーザーのログインIDのリストをSQL文で持ってくる
			List<User> login_idList = userDao.listLogin_id(userId);
			for (User loginList : login_idList) {
				loginId = request.getParameter("loginId");
				if (loginId.equals(loginList.getLogin_id())) {
					//System.out.println("ログインID登録失敗");
					err = -1;
					request.setAttribute("err", err);
					request.setAttribute("user", userDao.findByUserId(userId));
					request.getRequestDispatcher("WEB-INF/view/userEdit.jsp").forward(request, response);
				}
			}
		} catch (Exception e3) {
			// TODO 自動生成された catch ブロック
			throw new ServletException(e3);
		}
		String loginPass = null;
		if (!request.getParameter("loginPass").isEmpty()) {
			try {
				loginPass = userDao.makePasswordHash(request.getParameter("loginPass"));

			} catch (Exception e2) {
				// TODO 自動生成された catch ブロック
				throw new ServletException(e2);
			}
		}
		String groupName = request.getParameter("groupName");
		String beforeGroupName = request.getParameter("beforeGroupName");

		Integer groupId = 2;
		Event group = new Event();
		group.setGroupName(groupName);
		EventDao GroupDao = DaoFactory.createEventDao();
		try {
			//変更前のグループと変更後のグループが同じではないなら
			if (!groupName.equals(beforeGroupName)) {
				AttendDao attendDao = DaoFactory.createAttendDao();
				Attend attend_del = new Attend();
				attend_del.setAtttendUserId(userId);
				attendDao.attendDeleteUserId(attend_del);
			}
			Event id = GroupDao.findByGroupId(group);
			groupId = id.getGroupId();
		} catch (Exception e1) {
			// TODO 自動生成された catch ブロック
			System.out.println(groupId);
			throw new ServletException(e1);
		}

		User user = new User();
		user.setUserId(userId);
		user.setName(name);
		user.setTypeId(typeId);
		user.setLogin_id(loginId);
		try {
			if (loginPass != null) {
				user.setPass(loginPass);
			}
		} catch (Exception e) {
			throw new ServletException(e);
		}
		user.setGroupId(groupId);
		try {
			userDao.update(user);
			request.setAttribute("user", userDao.findByUserId(userId));
			request.getRequestDispatcher("WEB-INF/view/userEditComp.jsp").forward(request, response);

		} catch (Exception e) {
			System.out.println(e);
			throw new ServletException(e);
		}
	}

}
