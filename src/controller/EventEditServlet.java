package controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AttendDao;
import dao.DaoFactory;
import dao.EventDao;
import domain.Attend;
import domain.Event;

/**
 * Servlet implementation class EventEditServlet
 */
@WebServlet({ "/eventEdit", "/EventEditServlet" })
public class EventEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EventEditServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Event event = new Event();
		Integer eventId = Integer.valueOf(request.getParameter("eventId"));
		//		Integer eventId = 1;
		event.setEventId(eventId);
		request.setAttribute("err", 0);
		request.setAttribute("err2", 0);

		try {
			EventDao eventDao = DaoFactory.createEventDao();
			event = eventDao.findByEventId(event);
			request.setAttribute("event", event);

			if (request.getSession().getAttribute("loginUserType") == "1") {
				request.getRequestDispatcher("WEB-INF/view/eventEditAdmin.jsp").forward(request, response);

			} else {
				request.getRequestDispatcher("WEB-INF/view/eventEdit.jsp").forward(request, response);
			}

		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Integer eventId = Integer.parseInt(request.getParameter("eventId"));
		String title = request.getParameter("title");
		String START = request.getParameter("start");
		String END = request.getParameter("end");
		String place = request.getParameter("place");
		String groupName = request.getParameter("groupName");
		Integer groupId = 0;

		Event event = new Event();
		event.setEventId(eventId);

		Event group = new Event();
		group.setGroupName(groupName);

		EventDao GroupDao = DaoFactory.createEventDao();
		try {
			Event id = GroupDao.findByGroupId(group);
			groupId = id.getGroupId();
		} catch (Exception e1) {
			// TODO 自動生成された catch ブロック
			e1.printStackTrace();
		}

		String detail = request.getParameter("detail");
		Integer registeredById = Integer.parseInt(request.getParameter("registeredById"));

		//		Integer registeredById = Integer.valueOf((String) request.getSession().getAttribute("typeId"));
		//		Integer registeredById = 1;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		event.setEventId(eventId);
		event.setTitle(title);
		Date date1 = new Date();
		Date date2 = new Date();
		event.setPlace(place);
		event.setDetail(detail);
		event.setRegisteredById(registeredById);
		event.setGroupId(groupId);
		event.setGroupName(groupName);

		EventDao datech = DaoFactory.createEventDao();
		try {
			if (!datech.checkDate(START) || !datech.checkDate(END)) {

				if (!datech.checkDate(START) && !datech.checkDate(END)) {
					request.setAttribute("err", -3);
					request.setAttribute("err2", -3);
				} else if (!datech.checkDate(START)) {
					request.setAttribute("err", -3);
					request.setAttribute("err2", 0);
				} else if (!datech.checkDate(END)) {
					request.setAttribute("err", 0);
					request.setAttribute("err2", -3);
				}
				EventDao eventDao = DaoFactory.createEventDao();
				try {
					Event event2 = eventDao.findByEventId(event);
					request.setAttribute("event", event2);
					request.getRequestDispatcher("WEB-INF/view/eventEdit.jsp").forward(request, response);
				} catch (Exception e) {
					// TODO 自動生成された catch ブロック
					throw new ServletException(e);
				}

			}
		} catch (

		Exception e1) {
			// TODO 自動生成された catch ブロック
			e1.printStackTrace();
		}
		try {
			date1 = sdf.parse(START);
			event.setStart(date1);
		} catch (ParseException e2) {
			e2.printStackTrace();
		}

		try {
			date2 = sdf.parse(END);
			event.setEnd(date2);
		} catch (ParseException e3) {
			e3.printStackTrace();
		}

		if ((Integer) request.getSession().getAttribute("loginUserType") != 1) {
			Date now = new Date();
			if (date1.before(now) || date2.before(now) || date2.before(date1)) {
				if (date1.before(now) && date2.before(now)) {
					request.setAttribute("err", -1);
					request.setAttribute("err2", -1);
				} else if (date1.before(now)) {
					request.setAttribute("err", -1);
					request.setAttribute("err2", 0);
				} else if (date2.before(now)) {
					request.setAttribute("err", 0);
					request.setAttribute("err2", -1);
				} else if (date2.before(date1)) {
					request.setAttribute("err", 0);
					request.setAttribute("err2", -2);
				}
				EventDao eventDao = DaoFactory.createEventDao();
				try {
					eventDao.findByEventId(event);
					request.setAttribute("event", event);
					request.getRequestDispatcher("WEB-INF/view/eventEdit.jsp").forward(request, response);
				} catch (Exception e) {
					// TODO 自動生成された catch ブロック
					throw new ServletException(e);
				}
				//			event.setStart(null);
				//			event.setStart(null);
				//			request.setAttribute("event", event);
				//			request.getRequestDispatcher("WEB-INF/view/eventEdit.jsp").forward(request, response);
			} else {

				try {
					EventDao eventDao = DaoFactory.createEventDao();
					Event beforeEV = eventDao.findByEventId(event);
					eventDao.update(event);
					Event afterEV = eventDao.findByEventId(event);

					if (!(beforeEV.getGroupId() == afterEV.getGroupId())) {
						Attend attend = new Attend();
						attend.setAttendEventId(beforeEV.getEventId());

						AttendDao attendDao = DaoFactory.createAttendDao();
						attendDao.attendDeleteEventId(attend);
					}
					//					Event event2 = new Event();
					//					event2.setEventId(eventId);
					//					eventDao.findByEventId(event2);
					//					request.setAttribute("event", event2);
					request.getRequestDispatcher("WEB-INF/view/eventEditComp.jsp").forward(request, response);

				} catch (Exception e) {
					throw new ServletException(e);
				}
			}

		} else {
			try {
				EventDao eventDao = DaoFactory.createEventDao();
				Event beforeEV = eventDao.findByEventId(event);
				eventDao.update(event);
				Event afterEV = eventDao.findByEventId(event);

				if (!(beforeEV.getGroupId() == afterEV.getGroupId())) {
					Attend attend = new Attend();
					attend.setAttendEventId(beforeEV.getEventId());

					AttendDao attendDao = DaoFactory.createAttendDao();
					attendDao.attendDeleteEventId(attend);
				}
				Event event2 = new Event();
				event2.setEventId(eventId);
				eventDao.findByEventId(event2);
				request.setAttribute("event", event2);
				request.getRequestDispatcher("WEB-INF/view/eventEditComp.jsp").forward(request, response);

			} catch (Exception e) {
				throw new ServletException(e);
			}

		}

	}

}