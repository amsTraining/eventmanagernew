package domain;

public class Attend {

	private Integer attendId;

	private String loginId;

	private String attendtf;
	private Integer eventId;

	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public String getAttendtf() {
		return attendtf;
	}

	public void setAttendtf(String attendtf) {
		this.attendtf = attendtf;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public Integer getAttendId() {
		return attendId;
	}

	public Integer getAtttendId() {
		return attendId;
	}

	public void setAttendId(Integer attendId) {
		this.attendId = attendId;
	}

	private Integer atttendUserId;

	private Integer attendEventId;

	public Integer getAtttendUserId() {
		return atttendUserId;
	}

	public void setAtttendUserId(Integer atttendUserId) {
		this.atttendUserId = atttendUserId;
	}

	public Integer getAttendEventId() {
		return attendEventId;
	}

	public void setAttendEventId(Integer attendEventId) {
		this.attendEventId = attendEventId;
	}

}
