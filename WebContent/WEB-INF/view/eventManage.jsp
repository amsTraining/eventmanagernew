<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.HashMap"%>
<%
	request.setCharacterEncoding("UTF-8");
	String id = null;
	if (request.getParameter("id") != null) {//idに値が入っていればその値をidに持たせる
		id = request.getParameter("id");
	} else {
		id = "0";
	} ;
%>
<%
	String ac = null;
	if (request.getParameter("page") != null) {//pageに値が入っていればその値をacに持たせる
		ac = request.getParameter("page");
	} else {
		ac = "0";
	}
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<!-- ▼ Bootstrap -->
<link href="${pageContext.request.contextPath}/css/style.css"
	rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/sticky-footer.css"
	rel="stylesheet">
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"
	rel="stylesheet" />
<script src="${pageContext.request.contextPath}/js/jquery-3.3.1.min.js"></script>



<script>
	//サーブレットへ送信するためにselectの値をhiddenにセットする
	$(document).ready(function() {

		//選択した部署IDをhiddenにセットする
		$('#groups').change(function() {
			var groupId = $('option:selected').val();
			$('#setGroup').val(groupId);

			//選択した会議室IDをhiddenにセットする
		});
		$('#rooms').change(function() {
			var roomId = $('option:selected').val();
			$('#setRoom').val(groupId);

		});
	});
</script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

<script
	src="${pageContext.request.contextPath}/js/jquery.tablesorter.min.js"></script>
<script src="${pageContext.request.contextPath}/js/dropdown.js"></script>
<script src="${pageContext.request.contextPath}/js/header.js"></script>
<title>イベント管理</title>
</head>
<body>
	<div class="wrapper container">
		<!-- ▼ ヘッダ -->
		<jsp:include page="header.jsp"><jsp:param
				name="activePage" value="EVENT" /></jsp:include>
		<main class="main-contents col-sm-10 col-sm-offset-1">

		<article>
			<h1>イベント一覧</h1>
			<div class="row">
				<div class="col-xs-4">
					<nav class="clearfix">
						<ul class="pagination pull-left">
							<li class="page-item"><a aria-level="前ページへ"
								href="EventManageServlet?id=<%=id%>
		&pg=<%if (Integer.parseInt(ac) <= 1) {%><%=1%><%} else {%><%=Integer.parseInt(ac) - 1%><%}%>"><span
									aria-hidden="true">≪</span></a></li>

							<%
								for (int i = 1; i <= Integer.parseInt(id); i++) {
									String u = "eventManage.jsp?id=" + id + "&page=" + Integer.toString(i);
									if (i == Integer.parseInt(ac)) {
							%>
							<li class="active">
								<%
									} else {
								%>

							<li>
								<%
									}
								%> <a href="EventManageServlet?id=<%=id%>&pg=<%=i%>"><%=i%></a>
							</li>

							<%
								}
							%><li><a aria-label="次のページへ"
								href="EventManageServlet?id=<%=id%>&pg=<%if (Integer.parseInt(ac) >= (Integer.parseInt(id))) {%><%=id%><%} else {%><%=Integer.parseInt(ac) + 1%><%}%>"><span
									aria-hidden="true">≫</span></a></li>
						</ul>


					</nav>

				</div>
			</div>

			<form class="form-horizontal"  action="EventRegistServlet" name="outputFrmCsv" method="get" >

				<div class="form-group">
					<label class="control-label col-sm-2 " for="unSetAccountList">開催部署</label>
					<div class="col-sm-3">
						<select name="groups" id="groups" class="form-control ">
							<%
								HashMap<Integer, String> groupMap = (HashMap<Integer, String>) request.getAttribute("groupMap");
								for (HashMap.Entry<Integer, String> entry : groupMap.entrySet()) {
							%>
							<option value=<%=entry.getKey()%>>
								<%=entry.getValue()%>
							</option>
							<%
								}
							%>
						</select>
					</div>

					<label class="control-label col-sm-2" for="unSetAccountList">会議室</label>
					<div class="col-sm-3">
						<select name="rooms" id="rooms" class="form-control">
							<%
								HashMap<Integer, String> roomMap = (HashMap<Integer, String>) request.getAttribute("roomMap");
								for (HashMap.Entry<Integer, String> entry : roomMap.entrySet()) {
							%>
							<option value=<%=entry.getKey()%>>
								<%=entry.getValue()%>
							</option>
							<%
								}
							%>
						</select>
					</div>

					<div class="col-sm-2">
						<input type="submit" id="btnCSV" class="btn btn-primary" value="検索" />
					</div>
			</div>

</form>


			<div class="table-responsive">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th class="col-md-3">タイトル</th>
							<th class="col-md-4">開始日時</th>
							<th class="col-md-2">場所</th>
							<th class="col-md-2">対象グループ</th>
							<th class="col-md-1">詳細</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${EventList}" var="Event">
							<tr>
								<td class="col-md-3"><c:out value="${Event.title}" /> <c:if
										test="${ Event.attendtf}">
										<span class="label label-danger">参加</span>
									</c:if></td>
								<td class="col-md-4"><fmt:parseDate value="${Event.start }"
										pattern="yyyy-MM-dd HH:mm:ss" var="start" /> <fmt:formatDate
										value="${start }" var="startFormat"
										pattern="yyyy年MM月dd日(E) HH時mm分" /> <c:out
										value="${startFormat}" /></td>
								<td class="col-md-2"><c:out value="${Event.place}" /></td>
								<td class="col-md-2"><c:out value="${Event.groupName}" /></td>
								<td class="col-md-1">
									<form action="EventDetail" method="get">
										<button type="submit" class="btn btn-default"
											value="${Event.eventId }" name="key">詳細</button>
									</form>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<div class="row">
				<div class="col-xs-2">
					<form action="EventRegistServlet" method="get">
						<input type="submit" class="btn btn-primary" value="イベントの登録" />
					</form>
				</div>
				<div class="col-xs-2">
					<form action="EventRegistServlet" name="outputFrmCsv" method="get"
						id="formCSV" onclick="setHidden()">
						<input type="hidden" id="setGroup" name="setGroup"> <input
							type="hidden" id="setRoom" name="setRoom"> <input
							type="submit" id="btnCSV" class="btn btn-primary" value="CSV出力" />
					</form>
				</div>
			</div>

		</article>
		</main>
	</div>
	<br>
	<!-- ▼ フッタ -->
	<footer class="footer">
		<p class="text-center text-muted">© icloud 2018 Inc.</p>
	</footer>

</body>
</html>