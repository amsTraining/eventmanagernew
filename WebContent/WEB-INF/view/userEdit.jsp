﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー管理</title>
<!-- ▼ Internet Explorer … 表示モード-＞常に標準モード -->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!-- ▼ デバイス(スマホ)対応 … 幅-＞端末の画面幅に合わせる ピクセル比率-＞CSSの記述と端末が1：1 -->
<meta name="viewport" content="width=device-width, initial-scale=1" />
<!-- ▼ Bootstrap -->
<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/sticky-footer.css" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" />
<!-- ▼ jQuery … CDNから取得 -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
	integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	crossorigin="anonymous"></script>
<!-- ▼ jQuery … jQueryの定義に失敗(CDNから読込めない?)した場合はローカルのファイルを読込む -->
<script>if (!window.jQuery){ document.write('<script src="${pageContext.request.contextPath}/js/jquery-3.3.1.min.js"><\/script>'); }</script>
<script
src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery-3.3.1.min.js"></script>
<script src="${pageContext.request.contextPath}/js/dropdown.js"></script>
<script src="${pageContext.request.contextPath}/js/header.js"></script>
<script>
 $(document).ready(function(){
	 $('form').submit(function(){
		var name=$('input[name=name]').val();
		var loginId=$('input[name=loginId]').val();
		var groupName=$('input[name=groupName]').val();
		if(name==''){
			alert('氏名を入力してください');
			return false;
		}
		if(loginId==''){
			alert('ログインIDを入力してください');
			return false;
		}
		if(groupName==''){
			alert('所属グループを選んでください');
			return false;
		}
	});
});
 </script>
<%String loginName =(String)session.getAttribute("loginName"); %>
</head>
<body>
	<div class="wrapper container">
		<!-- ▼ ヘッダ -->
		<jsp:include page="header.jsp"><jsp:param name="activePage" value="USERMG" /></jsp:include>
		<!-- ▼ メインコンテンツ -->
		<main class="main-contents col-sm-10 col-sm-offset-1">
		<article>
			<h2>ユーザ編集</h2>
			<!-- ▼ 入力フォーム -->
			<form action="UserEditServlet" method="post">
				<!-- ▼ 情報表示 -->
				<p>
					<input type="hidden" name="userId" class="form-control"
						value="${user.userId }" />
				</p>
				<label>氏名 (必須)</label>
				<p>
					<input type="text" name="name" class="form-control"
						value="${user.name }" />
				</p>
				<label>ログインID (必須)</label>
				<p>
					<%
					int err = (Integer) request.getAttribute("err");
					if (err == -1) {
				%>

				<p class="text-danger">すで登録済みのログインIDのため登録ができません。</p>
				<%
					}
				%>
				<input type="text" name="loginId" class="form-control"
					value="${user.login_id }" />
				</p>
				<label>パスワード (変更の場合のみ)</label>
				<p>
					<input type="password" name="loginPass" class="form-control"
						value="" />
				</p>
				<label>所属グループ (必須)</label>
				<div class="input-group choose-group js-choose-group">
					<input type="text" readonly name="groupName"
						class="form-control js-group-input" value="${user.groupName}" /> <span
						class="input-group-btn">
						<button class="btn btn-default dropdown-toggle"
							data-toggle="dropdown">
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu pull-right">
							<li><a class="js-group-name">人事部</a></li>
							<li><a class="js-group-name">技術部</a></li>
							<li><a class="js-group-name">総務部</a></li>
							<li><a class="js-group-name">営業部</a></li>
						</ul>
					</span>
				</div>
				<c:if test="${user.typeId!=1 }">
				<div class="form-group row form-check">
					<p>ユーザー権限</p>
					<input type="radio" name="typeId" value="2" checked="checked" />一般ユーザー
					<input type="radio" name="typeId" value="1" />管理者
				</div>
				</c:if>
				<c:if test="${user.typeId=='1'}">
				<input type="hidden" name="typeId" value="1"/>
				</c:if>
				<!-- ▼ ユーザ編集のキャンセルボタン -->
				<a href="UserDetailServlet?key=${user.userId }"
					class="btn btn-default">キャンセル</a>
				<button type="submit" name="beforeGroupName" value="${user.groupName}" class="btn btn-primary">保存</button>

			</form>
		</article>
		</main>
	</div>
	<!-- ▼ フッタ -->
	<footer class="footer">
		<p class="text-center text-muted">© icloud 2018 Inc.</p>
	</footer>

</body>
</html>