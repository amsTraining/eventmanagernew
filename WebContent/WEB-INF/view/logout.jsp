<%@ page language="java"
contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%--
ページディレクティブ
属性：language、contentType、pageEncoding
--%>

<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1"/>

<!-- ▼ Bootstrap -->
<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/sticky-footer.css" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" />
<!-- ▼ jQuery … CDNから取得 -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
	integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	crossorigin="anonymous"></script>
<!-- ▼ jQuery … jQueryの定義に失敗(CDNから読込めない?)した場合はローカルのファイルを読込む -->
<script>if (!window.jQuery){ document.write('<script src="${pageContext.request.contextPath}/js/jquery-3.3.1.min.js"><\/script>'); }</script>
<script
src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery-3.3.1.min.js"></script>
<script src="${pageContext.request.contextPath}/js/dropdown.js"></script>
<script src="${pageContext.request.contextPath}/js/header.js"></script>
<title>ログアウト</title>

</head>

<body>
<div class="wrapper container">

<header>
<nav class="navbar navbar-default navbar-fixed-top">
<div class="navbar-header">

<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#pageNavbar">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>

<a href="" class="navbar-brand">Event Manager</a>

</div>
</nav>
</header>

<main class="main-contents col-sm-10 col-sm-offset-1">
<h2>ログアウト</h2>
<p>ログアウトしました。</p>
<a href="LoginServlet">ログイン画面に戻る</a>
</main>
</div>
 <footer class="footer">
      <p class="text-center text-muted">© icloud 2018 Inc.</p>
    </footer>

</body>

</html>