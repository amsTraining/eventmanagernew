﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8" />
<!-- ▼ Internet Explorer … 表示モード-＞常に標準モード -->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!-- ▼ デバイス(スマホ)対応 … 幅-＞端末の画面幅に合わせる ピクセル比率-＞CSSの記述と端末が1：1 -->
<meta name="viewport" content="width=device-width, initial-scale=1" />
<!-- ▼ Bootstrap -->
<!-- ▼ Bootstrap -->
<link href="${pageContext.request.contextPath}/css/style.css"
	rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/sticky-footer.css"
	rel="stylesheet">
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"
	rel="stylesheet" />
<!-- ▼ jQuery … CDNから取得 -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
	integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	crossorigin="anonymous"></script>
<!-- ▼ jQuery … jQueryの定義に失敗(CDNから読込めない?)した場合はローカルのファイルを読込む -->
<script>
	if (!window.jQuery) {
		document
				.write('<script src="${pageContext.request.contextPath}/js/jquery-3.3.1.min.js"><\/script>');
	}
</script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery-3.3.1.min.js"></script>
<script src="${pageContext.request.contextPath}/js/dropdown.js"></script>
<script src="${pageContext.request.contextPath}/js/header.js"></script>
<script>
	$(document).ready(function() {
		
		
		$('form').submit(function() {
			var login_id = $('input[name=login_id]').val();
			var login_pass = $('input[name=login_pass]').val();

			if (login_id == '') {
				alert('ログインIDを入力してください');
				return false;
			}
			if (login_pass == '') {
				alert('パスワードを入力してください');
				return false;
			}

		});

		//社員IDの変更によって社員名と部署名をセットする
		$('select').change(function() {
			var result = $(this).val().split(',');
			$('#EmployeeName').text(result[0]);
			$('#DepartMentName').text(result[1]);

		});
	});
</script>
<body>
	<div class="wrapper container">
		<!-- ▼ ヘッダ -->
		<jsp:include page="header.jsp"><jsp:param
				name="activePage" value="USERMG" /></jsp:include>
		<!-- ▼ メインコンテンツ -->
		<main class="main-contents col-sm-10 col-sm-offset-1">
		<article>
			<h2>ユーザ登録</h2>
			<!-- ▼ 入力フォーム -->
			<form action="UserRegistServlet" method="post">
				<c:if test="${!empty errMsg}">
					<p class="alert alert-warning" role="alert">
						<c:out value="${errMsg}" />
					</p>
				</c:if>

				<div class="form-group">

					<label class="col-sm-3 control-label" for="unSetAccountList">社員ID</label>
					<p>
						<select name="employee_id" class="form-control col-sm-9"
							id="unSetAccountList">
							<c:forEach items="${unSetAccountList}" var="User"
								varStatus="loop">
								<option
									value="${User.name},${User.groupName},${User.employeeId}">
									<c:out value="${User.employeeId}" /></option>
							</c:forEach>
						</select>
					</p>


					<label class="col-sm-3 control-label">名前</label>
					<p class="form-control col-sm-9" id="EmployeeName"></p>

					<br> <label class="col-sm-3 control-label">部署名</label>
					<p class="form-control col-sm-9" id="DepartMentName"></p>


					<label class="col-sm-3 control-label">ログインID (必須)</label>


					<p>
						<input type="text" name="login_id" class=" col-sm-9 form-control"
							placeholder="ログインID" />
					</p>


					<label class="col-sm-3 control-label">パスワード (必須)</label>
					<p>
						<input type="password" name="login_pass" class="form-control"
							placeholder="パスワード" />
					</p>


					<div class="form-group row form-check">
						<label class="col-sm-3 control-label">ユーザー権限</label> <input
							type="radio" name="type_id" value="1" checked="checked" />一般ユーザー
						<input type="radio" name="type_id" value="2" />管理者
					</div>

				</div>
				<!-- ▼ ユーザの登録キャンセルボタン -->

				<a href="UserManageServlet" class="btn btn-default">キャンセル</a>
				<!-- ▼ ユーザの登録ボタン -->
				<!--
            <button type="submit" name="" value="" class="btn btn-primary">登録</button>
            -->
				<input type="submit" value="登録" class="btn btn-primary">
			</form>
		</article>
		</main>
	</div>
	<!-- ▼ フッタ -->
	<footer class="footer">
		<p class="text-center text-muted">© icloud 2018 Inc.</p>
	</footer>

</body>
</html>
