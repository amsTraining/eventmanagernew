<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="domain.Attend"%>
<%@ page import="java.util.List"%>
<%
	request.setCharacterEncoding("UTF-8");
	String id = null;
	if (request.getParameter("id") != null) {
		id = request.getParameter("id");
	} else {
		id = "0";
	} ;
%>
<%
	String ac = null;
	if (request.getParameter("page") != null) {
		ac = request.getParameter("page");
	} else {
		ac = "0";
	}
%>
<!DOCTYPE html>
<html lang="ja">
<head>

<meta charset="utf-8" />
<!-- ▼ Internet Explorer … 表示モード-＞常に標準モード -->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!-- ▼ デバイス(スマホ)対応 … 幅-＞端末の画面幅に合わせる ピクセル比率-＞CSSの記述と端末が1：1 -->
<meta name="viewport" content="width=device-width, initial-scale=1" />
<!-- ▼ Bootstrap -->
<link href="${pageContext.request.contextPath}/css/style.css"
	rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/sticky-footer.css"
	rel="stylesheet">
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"
	rel="stylesheet" />
<!-- ▼ jQuery … CDNから取得 -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
	integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	crossorigin="anonymous"></script>
<!-- ▼ jQuery … jQueryの定義に失敗(CDNから読込めない?)した場合はローカルのファイルを読込む -->
<script>
	if (!window.jQuery) {
		document
				.write('<script src="${pageContext.request.contextPath}/js/jquery-3.3.1.min.js"><\/script>');
	}
</script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery-3.3.1.min.js"></script>
<script src="${pageContext.request.contextPath}/js/dropdown.js"></script>
<script src="${pageContext.request.contextPath}/js/header.js"></script>

<title>イベント管理</title>
</head>
<body>
	<div class="wrapper container">
		<!-- ▼ ヘッダ -->
		<jsp:include page="header.jsp"><jsp:param name="activePage" value="TODAY" /></jsp:include>
		<!-- ▼ メインコンテンツ -->
		<main class="main-contents col-sm-10 col-sm-offset-1">
		<article>
			<h1>本日のイベント</h1>
			<!-- ▼ 入力フォーム -->
			<!-- ▼ ページ遷移 -->
			<nav class="clearfix">
				<ul class="pagination pull-left">
					<li><a
						href="TodaysEventServlet?id=<%=id%>&pg=<%if (Integer.parseInt(ac) <= 1) {%><%=1%><%} else {%><%=Integer.parseInt(ac) - 1%>
                  <%}%>"
						aria-label="前のページへ"> <span aria-hidden="true">≪</span>
					</a></li>

					<%
						for (int i = 1; i <= Integer.parseInt(id); i++) {
							String u = "ChangePageServlet?id=" + id + "&pg=" + Integer.toString(i);
							if (i == Integer.parseInt(ac)) {
					%>
					<li class="active">
						<%
							} else {
						%>

					<li>
						<%
							}
						%> <a href="TodaysEventServlet?id=<%=id%>&pg=<%=i%>"><%=i%></a>
					</li>

					<%
						}
					%>
					<li><a
						href="TodaysEventServlet?id=<%=id%>&pg=<%if (Integer.parseInt(ac) >= (Integer.parseInt(id))) {%><%=id%><%} else {%><%=Integer.parseInt(ac) + 1%>
                  <%}%>"
						aria-label="次のページへ"> <span aria-hidden="true">≫</span>
					</a></li>
				</ul>
			</nav>
			<!-- ▼ 情報表示 -->
			<div class="table-responsive">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th class="col-md-3">タイトル</th>
							<th class="col-md-4">開始日時</th>
							<th class="col-md-2">場所</th>
							<th class="col-md-2">対象グループ</th>
							<th class="col-md-1">詳細</th>
						</tr>
					</thead>
					<tbody>
						<tr>

							<c:forEach items="${eventList}" var="event">
								<tr>
								<tr>
									<td class="col-md-3"><c:out value="${ event.title }" /> <c:if
											test="${ event.attendtf}">
											<span class="label label-danger">参加</span>
										</c:if></td>
									<td class="col-md-4"><fmt:parseDate
											value="${event.start }" pattern="yyyy-MM-dd HH:mm:ss"
											var="start" /> <fmt:formatDate value="${start }"
											var="startFormat" pattern="yyyy年MM月dd日(E) HH時mm分" /> <c:out
											value="${startFormat}" /></td>
									<td class="col-md-2"><c:out value="${ event.place }" /></td>
									<td class="col-md-2"><c:out value="${ event.groupName }" /></td>
									<td class="col-md-1">
										<!--
                      <button type="submit" name="" value="" class="btn btn-default">詳細</button>
                      -->

										<form action="EventDetail" method="get">
											<button type="submit" class="btn btn-default"
												value="${event.eventId }" name="key">詳細</button>
										</form>
									</td>
								</tr>
							</c:forEach>

						</tr>
					</tbody>
				</table>
			</div>
		</article>
		</main>
	</div>
	<!-- ▼ フッタ -->
	<footer class="footer">
		<p class="text-center text-muted">© icloud 2018 Inc.</p>
	</footer>

</body>
</html>
