-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: group2db
-- ------------------------------------------------------
-- Server version	5.7.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LOGIN_ID` varchar(50) NOT NULL,
  `LOGIN_PASS` varchar(255) NOT NULL,
  `NAME` varchar(50) NOT NULL,
  `TYPE_ID` int(11) NOT NULL,
  `GROUP_ID` int(11) NOT NULL,
  `CREATED` datetime NOT NULL,
  `employee_id` char(10) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `LOGIN_ID` (`LOGIN_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'ade','$2a$10$DrSYGrlVYfZWRQp67zTaoOlHnMdTpu8VVeaU4XTJ.cjZSsGUI.zzG','あで',2,1,'2019-05-31 09:49:48',''),(2,'ka','pass18','香',2,2,'2019-05-31 09:49:50',''),(3,'sato','$2a$10$aHccN8vi82NskXGwDlrIiuvkNd3OngQlPA4L.BcWrE6lmRzH/xhIG','佐藤五郎',2,2,'2019-05-22 15:38:06',''),(4,'umeda','$2a$10$aHccN8vi82NskXGwDlrIiuvkNd3OngQlPA4L.BcWrE6lmRzH/xhIG','梅田伸二',1,3,'2019-05-22 15:38:06',''),(5,'taketatsu','$2a$10$tN8FC0lXH6du.TjenSnVNOw6tYpXmaIVYz4/IEmrsgSkBQhh/To6S','竹達彩奈',1,4,'2019-05-30 15:35:10',''),(6,'hanazawa','$2a$10$LKyaKpPakYXxtr2ax5rx/O0ScvNMgqEziwkUomvzWAdDDV/LHcTn6','花澤香菜',2,2,'2019-05-30 15:35:18',''),(7,'minase','$2a$10$KTGy.2r1oOTZSgYRoCxwd.trr6zyhNX.JZBLopTgyonTIlA126zny','水瀬いのり',2,3,'2019-05-30 15:33:43',''),(8,'sakura','$2a$10$ZUcRmKJStoZX70mCUNtPb.EaTU4qxCphPWS4KSIq9qpooTwxfyajW','佐倉綾音',2,1,'2019-05-30 15:32:40',''),(9,'ito','$2a$10$xXndt.b03CIhvgsdRKiEz.qt8zHNGwcxo0r9.B2tA3bYbW3aMSFyS','伊藤美来',1,2,'2019-05-30 15:33:47',''),(10,'mikami','$2a$10$sBb1mws8ZZKirqtWDWYzuesC5IRM/jJ7dFQchX1lp9dDxP98YrYaq','三上辰夫',2,2,'2019-05-30 15:35:24',''),(11,'jackson','$2a$10$WiaZuXF0C1ThEh7bALTadePq/d81JMkyXQbaTSFXkqrk8OqFQPvDK','ジャクソン・サウザンド',1,3,'2019-05-30 15:35:26',''),(12,'hishida','$2a$10$WiaZuXF0C1ThEh7bALTadePq/d81JMkyXQbaTSFXkqrk8OqFQPvDK','菱田聖華',1,2,'2019-05-30 15:35:28',''),(13,'ide','$2a$10$16czmC6iNjxNPGwXAZmYfuWwA8IKSh3vG4HLKaKSdj8d8kOP8JJnq','井手絵里奈',2,4,'2019-05-30 15:35:31',''),(14,'sho','$2a$10$UjdFoSIvbGbxKugD.F0rd.mkuyfIaUOuR.F01g8qaMMNMZBPcQj1m','小学生',1,3,'2019-05-31 09:44:07','');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-06 17:01:26
